# Online Teaching Platform

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

### Tech

* Django rest framework
* React JS

### Backend Installation

Create a vitual environment

Install the dependencies and devDependencies and start the server.
```sh
$ cd backend
$ pip install -r requirements.txt
```

Ruuning the project...

```sh
$ python manage.py runserver
```

Verify the deployment by navigating to your server address in your preferred browser.

```sh
127.0.0.1:8000
```

### Frontend Installation


Install the dependencies and devDependencies and start the server.
```sh
$ cd frontend
$ npm install
```
Ruuning the project...

```sh
$ npm start
```
Verify the deployment by navigating to your server address in your preferred browser.

```sh
127.0.0.1:3000
```

License
----

MIT
