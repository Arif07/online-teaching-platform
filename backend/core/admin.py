from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from core.models import User


@admin.register(User)
class UserAdmin(UserAdmin):
    list_display = ('email',
                    'username',
                    'last_login',
                    'is_staff',
                    'date_joined')
    search_fields = ('email',
                     'username')
    readonly_fields = ('date_joined',
                       'last_login')

    filter_horizontal = ()
    fieldsets = ()
