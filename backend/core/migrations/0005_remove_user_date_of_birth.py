# Generated by Django 3.0.7 on 2020-07-28 06:32

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0004_student_teacher'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='user',
            name='date_of_birth',
        ),
    ]
