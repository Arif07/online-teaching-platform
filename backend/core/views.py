# allauth & dj_rest_auth
from rest_framework import generics
from allauth.socialaccount.providers.google.views import GoogleOAuth2Adapter
from dj_rest_auth.registration.views import SocialLoginView
from .models import User
from .serializers import UserSerializer


class GoogleLogin(SocialLoginView):
    adapter_class = GoogleOAuth2Adapter


class RegistrationView(generics.ListCreateAPIView):
    serializer_class = UserSerializer
    queryset = User.objects.all()
