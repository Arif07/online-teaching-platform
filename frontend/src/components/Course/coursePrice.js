import React from "react";

const CousePrice = () => {
    return (
        <div className="pl-5 pr-5">
            <div className="card">
                <div className="text-center bg-warning pt-2">
                    <h5 className="text-uppercase">Take This Course</h5>
                </div>
                <div className="card-body d-flex justify-content-center">
                    <div>
                        <strong class="price element-block font-lato">Price: £39.00</strong>
                        <hr></hr>
                        <ul className="list-unstyled">
                            <li><i className="fa fa-user icn no-shrink"></i> 199 Students</li>
                            <hr></hr>
                            <li><i className="fa fa-play-circle icn no-shrink"></i> Duration: 30 days</li>
                            <hr></hr>
                            <li><i className="fa fa-bullhorn icn no-shrink"></i> Lectures: 10</li>
                            <hr></hr>
                            <li><i className="fa fa-play-circle icn no-shrink"></i> Video: 12 hours</li>
                            <hr></hr>
                            <li><i className="fa fa-address-card icn no-shrink"></i> Certificate of Completion</li>
                        </ul>
                    </div>
                </div>

            </div>
        </div>
    )
}

export default CousePrice;
