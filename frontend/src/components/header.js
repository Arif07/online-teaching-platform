import React from 'react';

import { Link } from 'react-router-dom';

const Header = () => {
    return (
        <div className="container">
            <nav className="navbar navbar-expand-lg sticky">
                <Link className="navbar-brand logo" to={{ pathname: '/' }}>Home
                </Link>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse flex-row-reverse" id="navbarNavDropdown">
                    <ul className="navbar-nav">
                        <li className="nav-item ml-4">
                            <Link className="nav-link" to={{ pathname: '/login' }}>Login
                            </Link>
                        </li>
                        <li className="nav-item ml-4">
                            <Link className="nav-link" to={{ pathname: '/signup' }}>Signup
                            </Link>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
    );
};

export default Header;