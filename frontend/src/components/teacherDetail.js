import React, { useState, useEffect } from "react";
import { Carousel } from 'react-bootstrap';
import fakeData from "../FakeData/fakeData";
import image from "../assets/teacher.jpg";
import { Link } from "react-router-dom";
import Header from "./header";
import CourseDescription from "../components/Course/courseDescription";
import CoursePrice from "../components/Course/coursePrice";


const TeacherList = () => {

    const [teacher, setTeacher] = useState([]);

    useEffect(() => {
        setTeacher(fakeData);
    }, [])

    return (
        <div>
            <Header />
            <div className="container teacher-detail">
                <div className="row">
                    <div className="col-md-8">
                        <Carousel>
                            <Carousel.Item>
                                <iframe className="w-100 resume-video" src="https://www.youtube.com/embed/d0yGdNEWdn0">
                                </iframe>
                            </Carousel.Item>
                            <Carousel.Item>
                                <iframe className="w-100 resume-video" src="https://www.youtube.com/embed/d0yGdNEWdn0">
                                </iframe>
                            </Carousel.Item>
                        </Carousel>
                    </div>
                    <div className="col-md-4">
                        <CoursePrice />
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-8">
                        <CourseDescription />
                    </div>
                    <div className="col-md-4">

                    </div>
                </div>
            </div>
        </div>
    )
}

export default TeacherList;