dj-database-url==0.5.0
Django==3.0.7
django-filter==2.3.0
django-heroku==0.3.1
djangorestframework==3.11.0
djangorestframework-simplejwt==4.4.0
gunicorn==20.0.4
Pillow==7.1.2
psycopg2==2.8.5
PyJWT==1.7.1
pytz==2020.1
sqlparse==0.3.1
whitenoise==5.1.0
#auth
dj-rest-auth==1.1.0
django-allauth==0.42.0
django-braces==1.14.0
social-auth-core==3.3.3
django-templated-mail==1.1.1
